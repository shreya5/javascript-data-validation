'use strict';

var express = require('express');
var router = express.Router();
var MeasurementCtrl = require('./controllers/measurement.server.controller');
var StatsCtrl = require('./controllers/stat.server.controller');

router.post('/measurements', MeasurementCtrl.insertNewMetric);

/* Get based on timestamp */
router.get(/measurements\/(\d{1,4}.+[zZ]$)/, MeasurementCtrl.getBasedOnISODate);

/* Get based on day */
router.get(/measurements\/(\d{1,4}-\d\d-\d\d$)/, MeasurementCtrl.getBasedOnDay);

/* Update a specific metric */
router.put(/measurements\/(\d{1,4}.+[zZ]$)/, MeasurementCtrl.update);

/* Update a specific metric */
router.patch(/measurements\/(\d{1,4}.+[zZ]$)/, MeasurementCtrl.update);

router.delete(/measurements\/(.+[zZ]$)/, MeasurementCtrl.deleteMetric);

router.get('/stats', StatsCtrl.get);

/* To assist in testing */
router.post('/measurements/all', MeasurementCtrl.insertMultiple); //to enable testing

module.exports = router;