'use strict';
var _ = require('lodash');
var MetricModel = require('./metric.server.model');
var QueryStats = require('./queryStats.server.model');

function MetricsContainer() {
  var __ = {};

  Object.defineProperty(this, '__', {value: __});

}

MetricsContainer.prototype.add = function(item) {

  var singleMetric = new MetricModel(item);
  if (singleMetric.validator.status) {
    var timestamp = singleMetric.timestamp;
    if (!this.__[timestamp]) {
      this.__[timestamp] = singleMetric;
      return true;
    }
  }
  return false;

};

MetricsContainer.prototype.getBasedOnISODate = function(dateTime) {

  if (this.__[dateTime]) {
    return this.__[dateTime];
  } else {
    return null;
  }

};

MetricsContainer.prototype.getBasedOnDay = function(date) {

  var results = [];
  _.forEach(this.__, function(value, key) {
    if(key.includes(date, 0)) {
      results.push(value.clientData);
    }
  });

  return results;
};

MetricsContainer.prototype.update = function(key, data) {

  if (this.__[key]) {
    var newMetric = new MetricModel(data);

    if (newMetric.timestamp !== key) {
      return {status: 409};
    }

    if (newMetric.validator.status) {
      if (newMetric.temperature) {
       this.__[key].temperature = newMetric.temperature;
      }
      if (newMetric.dewPoint) {
        this.__[key].dewPoint = newMetric.dewPoint;
      }
      if (newMetric.precipitation) {
        this.__[key].precipitation = newMetric.precipitation;
      }
      return {status: 204};
    } else {
      return {status: 400};
    }
  }
    return {status: 404};
};

MetricsContainer.prototype.deleteMetric = function(key) {

 if (this.__[key]) {
   delete this.__[key];
   return {status: 204};
 } else {
   return {status: 404};
 }

};

function narrowByTimestamp(startTime, endTime, list) {
  var results = [];
  _.forEach(list, function(metric) {
    var timestamp = (new Date(metric.timestamp)).getTime();
    if (timestamp <= endTime && timestamp >= startTime) {
      results.push(metric);
    }
  });
  return results;
}

MetricsContainer.prototype.getStats = function(req) {

  var paramValidator = new QueryStats(req.query);
  var response = [];

  if (paramValidator.validator.status) {
    //first narrow-down based on date

    var startTime = (new Date(paramValidator.fromDateTime)).getTime();
    var endTime =  (new Date(paramValidator.toDateTime)).getTime();

    var results = narrowByTimestamp(startTime, endTime, this.__);

    var temperatureList = [];
    var dewPointList = [];
    var precipitationList = [];

    _.forEach(results, function(eachMetric) {
      if (eachMetric.temperature) {
        temperatureList.push(eachMetric.temperature);
      }
      if (eachMetric.dewPoint) {
        dewPointList.push(eachMetric.dewPoint);
      }
      if (eachMetric.precipitation) {
        precipitationList.push(eachMetric.precipitation);
      }
    });

    var metricsRequested = paramValidator.metrics;
    var statsRequested = paramValidator.stats;

    _.forEach(metricsRequested, function(metricName) {
      _.forEach(statsRequested, function(statName) {

        var listToUse;
        switch (metricName.toLowerCase()) {
          case 'temperature':
            listToUse = temperatureList;
            break;
          case 'dewpoint':
            listToUse = dewPointList;
            break;
          case 'precipitation':
            listToUse = precipitationList;
            break;
        }

        if (listToUse.length > 0) {
          var average;

          if (statName === 'average') {
            var sum = _.sum(listToUse);
            average = sum / listToUse.length;
            average = average.toFixed(1);
          }

          var result = {
            metric: metricName,
            stat: statName,
            value: average || _[statName.toLowerCase()](listToUse)
          };
          response.push(result);
        }

      });
    });

    return {status: 200, data: response};

  } else {
    return {status: 400, data: response};
  }

};

var container = new MetricsContainer();

module.exports = container;