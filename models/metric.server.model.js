'use strict';
var _ = require('lodash');
var Predicates = require('../services/Predicates.js');
var TestCase = require('../services/TestCase.js');
var Validator = require('../services/Validator.js');

/* Only timestamp is mandatory */
var rulesForTimestamp = [Predicates.isDefined(), Predicates.isValidDate()];
var rulesForTemperature = [Predicates.isNumeric()];
var rulesForDewPoint = [Predicates.isNumeric()];
var rulesForPrecipitation = [Predicates.isNumeric()];

function Metrics(initValue) {

  var __ = {
    timestamp: undefined,
    temperature: undefined,
    dewPoint: undefined,
    precipitation: undefined
  };

  _.assign(__, initValue);

  Object.defineProperty(this, '__', {value: __});

  var testForTimestamp = new TestCase(this.__, 'timestamp', rulesForTimestamp);
  var testForTemperature = new TestCase(this.__, 'temperature', rulesForTemperature);
  var testForDewPoint = new TestCase(this.__, 'dewPoint', rulesForDewPoint);
  var testForPrecipitation = new TestCase(this.__, 'precipitation', rulesForPrecipitation);

  var testGroup =  [testForTimestamp, testForTemperature, testForDewPoint, testForPrecipitation];

  this.__.validator = new Validator(testGroup);
  this.__.validator.runAllValidations();

}

Object.defineProperty(Metrics.prototype, 'validator', {
  get: function() {
    return this.__.validator;
  }
});

Object.defineProperties(Metrics.prototype, {
  'clientData' : {
    get: function() {
      return {
        timestamp: this.timestamp,
        temperature: this.temperature,
        dewPoint: this.dewPoint,
        precipitation: this.precipitation
      };
    }
  },
  'timestamp' : {
    get: function() {
      return this.__.timestamp;
    },
    set: function(value) {
      var testForTimestamp = new TestCase(value, null, rulesForTimestamp);
      testForTimestamp.run();
      if(testForTimestamp.status) {
        this.__.timestamp = value;
      }
    }
  },
  'temperature': {
    get: function() {
      return this.__.temperature;
    },
    set: function(value) {

      var testForTemperature = new TestCase(value, null, rulesForTemperature);
      testForTemperature.run();
      if(testForTemperature.status) {
        this.__.temperature = value;
      }

    }

  },
  'dewPoint': {
    get: function() {
      return this.__.dewPoint;
    },
    set: function(value) {

      var testForDewPoint = new TestCase(this.__, 'dewPoint', rulesForDewPoint);
      testForDewPoint.run();
      if(testForDewPoint.status) {
        this.__.dewPoint = value;
      }

    }

  },
  'precipitation': {
    get: function() {
      return this.__.precipitation;
    },
    set: function(value) {

      var testForPrecipitation = new TestCase(this.__, 'precipitation', rulesForPrecipitation);
      testForPrecipitation.run();
      if(testForPrecipitation.status) {
        this.__.precipitation = value;
      }

    }

  },
  'timestampInIntegerFormat': {
    get: function() {
      return parseInt(this.__.timestamp);  // will return date().getTime()
    }
  }
});

module.exports = Metrics;