'use strict';
var _ = require('lodash');
var Predicate = require('../services/Predicates');
var TestCase = require('../services/TestCase');
var Validator = require('../services/Validator');

function initializeValues(obj, initValue) {
  _.forEach(initValue, function (value, key) {
    switch (key) {
      case 'stat':
        obj.stat = _.concat([], value);
        break;
      case 'metric':
        obj.metric = _.concat([], value);
        break;
      case 'fromDateTime':
        obj.fromDateTime = value;
        break;
      case 'toDateTime':
        obj.toDateTime = value;
        break;
      default:
        break;
    }

  });
}

var rulesForStat = [Predicate.isDefined(), Predicate.hasMinSize(1), Predicate.hasValidStatValues()];
var rulesForMetric = [Predicate.isDefined(), Predicate.hasMinSize(1), Predicate.hasValidMetricValues()];
var rulesFromDateTime = [Predicate.isValidDate(), Predicate.isDefined()];
var rulesToDateTime = [Predicate.isValidDate(), Predicate.isDefined()];

function QueryStats(initValue) {
  var __ = {
    stat: [],
    metric: [],
    fromDateTime: null,
    toDateTime: null
  };


  initializeValues(__, initValue);

  Object.defineProperty(this, '__', {value: __});

  var testForStat = new TestCase(this.__, 'stat', rulesForStat);
  var testForMetric = new TestCase(this.__, 'metric', rulesForMetric);
  var testForFromDateTime = new TestCase(this.__, 'fromDateTime', rulesFromDateTime);
  var testForToDateTime = new TestCase(this.__, 'toDateTime' , rulesToDateTime);

  var testGroup = [testForStat, testForMetric, testForFromDateTime, testForToDateTime];

  this.__.validator = new Validator(testGroup);
  this.__.validator.runAllValidations();

}

Object.defineProperty(QueryStats.prototype, 'validator', {
  get: function() {
    return this.__.validator;
  }
});

Object.defineProperty(QueryStats.prototype, 'fromDateTime', {
  get: function() {
    return this.__.fromDateTime;
  }
});

Object.defineProperty(QueryStats.prototype, 'toDateTime', {
  get: function() {
    return this.__.toDateTime;
  }
});

Object.defineProperty(QueryStats.prototype, 'metrics', {
  get: function() {
    return this.__.metric;
  }
});

Object.defineProperty(QueryStats.prototype, 'stats', {
  get: function() {
    return this.__.stat;
  }
});

module.exports = QueryStats;
