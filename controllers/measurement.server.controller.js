'use strict';
var _ = require('lodash');
var MetricsContainerModel = require('../models/metricsContainer.server.model.js');

module.exports.insertNewMetric = function(req, res) {

  if (MetricsContainerModel.add(req.body)) {
    var temp = req.body.timestamp.toString().split('–').join('-');
    res.status(201).location('/measurements/' + temp).send();
  } else {
    res.status(400).send();
  }


};

module.exports.getBasedOnISODate = function(req, res) {
  var dateTime = new Date(req.params[0]);

  if (_.isDate(dateTime)) {
    var singleMetric = MetricsContainerModel.getBasedOnISODate(req.params[0]);
    if (singleMetric) {
      res.status(200).send(singleMetric.clientData);
    } else {
      res.status(404).send();
    }
  } else {
    res.status(404).send();
  }
};

module.exports.getBasedOnDay = function(req, res) {

  var dateString = req.params[0];
  var data = MetricsContainerModel.getBasedOnDay(dateString);

  if(data.length > 0) {
    res.status(200).send(data);
  } else {
    res.status(404).send();
  }
};


module.exports.update = function(req, res) {
  var key = req.params[0];
  var data = req.body;
  var result = MetricsContainerModel.update(key,data);
  res.status(result.status).send();
};

module.exports.deleteMetric = function(req, res) {
  var result = MetricsContainerModel.deleteMetric(req.params[0]);
  res.status(result.status).send();
};

/* Function written to assist in testing */
module.exports.insertMultiple = function(req, res) {

  _.forEach(req.body, function(metric) {
    MetricsContainerModel.add(metric);
  });

  res.status(200).send();
};

