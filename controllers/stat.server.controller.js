'use strict';
var MetricsContainerModel = require('../models/metricsContainer.server.model.js');

module.exports.get = function(req, res, next) {

 var result = MetricsContainerModel.getStats(req);
 res.status(result.status).send(result.data);

};
