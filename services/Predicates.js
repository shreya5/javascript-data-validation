'use strict';
var _ = require('lodash');

module.exports.isNumeric = function() {

  return function(value) {
    if (!value) {
      return true;
    }

    if(_.isNumber(value)) {
      return true;
    } else {
      return false;
    }

  };

};


module.exports.isDefined = function() {
  return function(value) {
    if (!_.isUndefined(value)  && !_.isNull(value)) {
      return true;
    } else {
      return false;
    }
  };
};


module.exports.isValidDate = function() {
  return function(value) {
    if (!value) {
      return true;
    }
    var temp = new Date(value);

    if(_.isDate(temp)) {
      return true;
    } else {
      return false;
    }

  };
};

module.exports.hasMinSize = function() {
  return function(value) {
    if (!value) {
      return true;
    }
    if (value.length > 0) {
      return true;
    } else {
      return false;
    }
  };
};

module.exports.hasValidStatValues = function(){

  var validValues = ['MIN', 'MAX', 'AVERAGE'];

  return function(list) {

    if (!list) {
      return true;
    }

    if (!Array.isArray(list)) {
      return false;
    }

    var validationStatus = true;
    _.forEach(list, function(stat) {
      if (validValues.indexOf(stat.toUpperCase()) < 0) {
        validationStatus = false;
      }
    });

    return validationStatus;

  };

};

module.exports.hasValidMetricValues = function() {

  var validValues = ['TEMPERATURE', 'DEWPOINT', 'PRECIPITATION'];

  return function(list) {

    if (!list) {
      return true;
    }

    if (!Array.isArray(list)) {
      return false;
    }

    var validationStatus = true;
    _.forEach(list, function(stat) {
      if (validValues.indexOf(stat.toUpperCase()) < 0) {
        validationStatus = false;
      }
    });

    return validationStatus;

  };

};
