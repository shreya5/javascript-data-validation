'use strict';
module.exports.logErrors = function(err, req, res, next) {
  console.error(err.stack);
  next(err);
};

module.exports.clientErrorHandler = function(err, req, res, next) {
  if (req.xhr) {
    res.status(500).send('Something blew up!');
  } else {
    next(err);
  }
};

module.exports.errorHandler = function errorHandler(err, req, res, next) {
  res.status(500);
  res.send('Something Broke!! :(');
};
