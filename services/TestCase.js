'use strict';
var _ = require('lodash');

function TestCase(expr, property, predicateList) {

  var __ = {};

  if (!Array.isArray(predicateList)) {
    throw new Error('Invalid argument. predicateList must be an array of predicates');
  }

  if (_.isUndefined(expr)) {
    throw new Error('You must supply a value for expr');
  }

  if (typeof(expr) === 'object' && !_.isUndefined(property)) {
    __.valueToTest = expr[property];
  } else {
    __.valueToTest = expr;
  }
  __.expr = expr;
  __.property = property;
  __.predicateList = predicateList;

  Object.defineProperty(this, '__', {value: __});
}

TestCase.prototype.run = function() {
  var outcome = true;
  var self = this;
  _.forEach(this.__.predicateList, function(conditionFn) {
    outcome = outcome && conditionFn(self.__.valueToTest);
  });
  this.status = outcome;
};

module.exports = TestCase;
