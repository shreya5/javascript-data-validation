'use strict';
var _ = require('lodash');

function Validator(testGroup) {
  this.testGroup = testGroup;
}

Validator.prototype.checkSingleProperty = function(propertyName) {

};

Validator.prototype.runAllValidations = function() {

  _.forEach(this.testGroup, function(eachTestCase) {
     eachTestCase.run();
  });

  var validityStatus = true;
  _.forEach(this.testGroup, function(eachTestCase) {
    validityStatus = validityStatus && eachTestCase.status;
  });

  this.status = validityStatus;

};

module.exports = Validator;