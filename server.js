'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var errorModel = require('./services/errorModel');
var routes = require('./routes');
var conf = require('./conf');


var app = express();
app.use(methodOverride());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

/* These should be the last items in the middleware before routes begin */
app.use(errorModel.logErrors);
app.use(errorModel.clientErrorHandler);
app.use(errorModel.errorHandler);

//** Get all routes **/
app.use('/', routes);

app.listen(conf.serverPort, function () {
  console.log('Listening on port ' + conf.serverPort);
});
